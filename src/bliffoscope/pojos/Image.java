package bliffoscope.pojos;

/**
 * POJO file containing Image related information. 
 * @author Rosh Lee
 *
 */
public class Image {
	private String targetType; 
	private int width; 
	private int height;
	private int totalOns;
	private char[][] pixels; 
	
	public Image(int width, int length, char[][] pixels) {
		this.targetType = null;
		this.width = width;
		this.height = length;
		this.totalOns = 0;
		this.pixels = pixels;
	}
	
	public Image(String targetType, int width, int length, int totalOns, char[][] pixels) {
		this.targetType = targetType;
		this.width = width;
		this.height = length;
		this.totalOns = totalOns;
		this.pixels = pixels;
	}
	
	public String getTargetType() {

		return targetType;
	}

	public void setTargetType(String targetType) {

		this.targetType = targetType;
	}
	
	public int getWidth() {
	
		return width;
	}

	
	public void setWidth(int width) {
	
		this.width = width;
	}

	
	public int getHeight() {
	
		return height;
	}

	
	public void setHeight(int height) {
	
		this.height = height;
	}

	
	public int getTotalOns() {
	
		return totalOns;
	}

	
	public void setTotalOns(int totalOns) {
	
		this.totalOns = totalOns;
	}

	
	public char[][] getPixels() {
	
		return pixels;
	}

	
	public void setPixels(char[][] pixels) {
	
		this.pixels = pixels;
	}
	
	/**
	 * This method is used to draw the value in pixels. 
	 */
	public void drawImage() {
		for (int i=0; i<pixels.length; i++) {
			for (int j=0; j<pixels[0].length; j++) {
				System.out.print(pixels[i][j]);
			}
			System.out.println();
		}
	}

	/**
	 * This method is used to return a total size of image.
	 * @return
	 */
	public int getTotalPixels() {

		return width*height;
	}

}
