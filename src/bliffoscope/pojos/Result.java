package bliffoscope.pojos;

/**
 * POJO containing Result information. 
 * @author Rosh Lee
 *
 */

public class Result {
	private String targetType; 
	private Coordinate coordinate; 
	private double matchRate; 
	
	public Result(String targetType, Coordinate coordinate, double matchRate) {
		this.targetType = targetType;
		this.coordinate = coordinate;
		this.matchRate = matchRate;
	}
	
	public String getTargetType() {
	
		return targetType;
	}

	
	public void setTargetType(String targetType) {
	
		this.targetType = targetType;
	}

	
	public Coordinate getCoordinate() {
	
		return coordinate;
	}

	
	public void setCoordinate(Coordinate coordinate) {
	
		this.coordinate = coordinate;
	}

	
	public double getMatchRate() {
	
		return matchRate;
	}

	
	public void setMatch(double matchRate) {
	
		this.matchRate = matchRate;
	}

}
