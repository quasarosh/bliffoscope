package bliffoscope.testers;

import java.io.FileNotFoundException;
import java.util.Queue;

import bliffoscope.ImageCreator;
import bliffoscope.TargetDetector;
import bliffoscope.constant.Constants;
import bliffoscope.pojos.Image;
import bliffoscope.pojos.Result;
import bliffoscope.utils.BliffoscopeUtils;
import bliffoscope.utils.ImageUtils;

/**
 * This is testing program to find a Starship images from given test data. 
 * Sample output format is as below, 
 * 
 * Found Starship Images: 3
 * ===========================
 * Target Type: Starship
 * Coordinates: (25, 25)
 * Match Rate: 72%
 * =========================== * @author Rosh Lee
 *
 */
public class StarshipDetectTester {

	private final static String TARGET_FILENAME = "Starship.blf";
	
	/**
	 * Testing program to find Starship image from TestData image. 
	 * @throws FileNotFoundException 
	 */
	public static void main(String[] args) throws FileNotFoundException {

		ImageCreator imgCreator = new ImageCreator();
		String filepath = Constants.LOCAL_PATH + Constants.TESTDATA_FILENAME;
		String targetType = "TestData";
		Image testData = imgCreator.extractImage(filepath, targetType);
		
		filepath = Constants.LOCAL_PATH + TARGET_FILENAME;
		targetType = "Starship";
		Image starship = imgCreator.extractImage(filepath, targetType);
		
		TargetDetector detector = new TargetDetector();
		detector.detectTarget(starship, testData);
		
		Queue<Result> resQueue = detector.getResultQueue();
		
		// To see the found images, please uncomment the line below
		//ImageUtils.drawFoundImages(resQueue, testData, starship);
		
		BliffoscopeUtils.printResults(resQueue, targetType);
		
	}
}
