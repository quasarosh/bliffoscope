package bliffoscope;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import bliffoscope.pojos.Image;
import bliffoscope.utils.BliffoscopeUtils;

/**
 * ImageCreator is used to create an Image object by using the context of given image file.
 * @author Rosh Lee
 *
 */
public class ImageCreator {
	/**
	 * This method is used to extract image information from given file, create an Image object 
	 * using the info, and return the created Image object.
	 * @param path
	 * @param targetType
	 * @return
	 * @throws FileNotFoundException
	 */
	public Image extractImage (String path, String targetType) throws FileNotFoundException {
		if (path == null || targetType == null || path.length() == 0 || targetType.length() == 0) {
			throw new IllegalArgumentException("Invalid value(s) input");
		}
		
		// Initialization
		Image image = null;
		BufferedReader br = null;

		try {
			String sCurrentLine;
			
			// Get an absolute path of local file system.
			String filePath = new File("").getAbsolutePath();
			br = new BufferedReader(new FileReader(filePath + path));

			int height = 0;
			int width = 0;
			int totalOns = 0;
			
			List<String> lines = new ArrayList<String>();
			
			// Calculate height and width of the streamed file context and 
			// add each line of file context to a list. 
			while ((sCurrentLine = br.readLine()) != null) {
				height++;
				width = Math.max(width, sCurrentLine.length());
				totalOns += BliffoscopeUtils.extractTotalOnsPerLine(sCurrentLine);
				lines.add(sCurrentLine);	
			}
			
			// Create 2X2 character array using the list containing file context
			char[][] pixels = BliffoscopeUtils.convertStrListToCharArr(lines, width);
			
			// Create an image using collected data.
			image = new Image(targetType, width, height, totalOns, pixels);
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				// While opening a file, if any exception happens, 
				// close the file (or disconnect a connection to the file)
				if (br != null) {
					br.close();
				}
			} catch (IOException ex) {
				ex.printStackTrace();
			}
		}
		return image;
	}
}
