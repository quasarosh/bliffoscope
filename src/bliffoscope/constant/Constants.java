package bliffoscope.constant;

/**
 * This file contains constant values.
 * @author Rosh Lee
 *
 */

public class Constants {
	
	public static final String LOCAL_PATH = "/resources/";  		// Local path to access image files. 
	public static final String TESTDATA_FILENAME = "TestData.blf";	// Test data file name
	
	public static final int QUEUE_CAPACITY = 100; 	// Priority Queue capacity
	public static final double PRECISION = 62;		// Minimum match rate to find the image 
	
}
