package bliffoscope;

import java.util.Comparator;
import java.util.PriorityQueue;
import java.util.Queue;

import bliffoscope.constant.Constants;
import bliffoscope.pojos.Coordinate;
import bliffoscope.pojos.Image;
import bliffoscope.pojos.Result;

/**
 * TargetDetector is used to detect target by comparing two images: target image and same size of 
 * test data image (partial area of the test data). The comparison will continue until it covers 
 * all area of test data. Once the comparison is done, and if both image has a certain percentage
 * of match rate (e.g., 62%), then add the result to the priority queue. 
 * 
 * @author Rosh Lee
 *
 */
public class TargetDetector {
	
	// Priority Queue having an ascending order of list for matchRate value of Result class.
	private Queue<Result> resultQueue = new PriorityQueue<Result>(Constants.QUEUE_CAPACITY,
			new Comparator<Result>() {
				public int compare (Result r1, Result r2) {
					return (int) (r2.getMatchRate() - r1.getMatchRate());
				}
			});
	
	/**
	 * This method is used to detect target image within given test data. 
	 * @param target
	 * @param data
	 */
	public void detectTarget(Image target, Image data) {
		// Exception handling
		if (data.getHeight() < target.getHeight() || data.getWidth() < target.getWidth()) {
			throw new IllegalArgumentException("Test data image size should be bigger than target image.");
		}
		
		int diffWidth = data.getWidth() - target.getWidth();
		int diffHeight = data.getHeight() - target.getHeight();
		
		// Compare a target image with all the portion of test data image. 
		for (int i=0; i<diffHeight; i++) {
			for (int j=0; j<diffWidth; j++) {
				compareImages(target, data, i, j);
			}
		}
	}
	
	/**
	 * This method is used to compare two images where one image is bigger than the other. 
	 * Based on match rate, partial area of a bigger image could be added into Priority Queue. 
	 * @param target
	 * @param data
	 * @param x
	 * @param y
	 */
	private void compareImages(Image target, Image data, int x, int y) {
		if (target == null || data == null || x < 0 || y < 0) {
			throw new IllegalArgumentException("Invalid value(s) input");
		}
		
		Coordinate coordinate = new Coordinate(x,y);
		int match = 0;
		int origY = y;
		for (int i=0; i<target.getHeight() && x<data.getHeight();i++,x++) {
			y = origY;
			for (int j=0; j<target.getWidth() && y<data.getWidth(); j++,y++) {
				if (target.getPixels()[i][j] == '+' && data.getPixels()[x][y] == '+') {
					match++;
				}
			}
		}
		
		// Calculate a match rate
		double matchRate = (double)match / (double)target.getTotalOns()*100;
		
		if (matchRate > Constants.PRECISION) {
			Result res = new Result(target.getTargetType(), coordinate, matchRate);
			resultQueue.add(res);
		}
	}
	
	public Queue<Result> getResultQueue() {
		return resultQueue;
	}
}
