package bliffoscope.utils;

import java.util.List;
import java.util.Queue;

import bliffoscope.pojos.Result;

/**
 * This is Utility file having multiple utility methods used this project. 
 * @author Rosh Lee
 *
 */
public class BliffoscopeUtils {
	/**
	 * This method is used to get a number of '+' in the given string. 
	 * @param str
	 * @return
	 */
	public static int extractTotalOnsPerLine(String str) {
		if (str == null || str.length() == 0) {
			throw new IllegalArgumentException("Invalid string is input.");
		}
		
		int totalOnsPerLine = 0;
		for (int i=0; i<str.length(); i++) {
			if (str.charAt(i) == '+') {
				totalOnsPerLine++;
			}
		}
		
		return totalOnsPerLine;
	}
	
	/**
	 * This method is used to convert String list to 2X2 character array.
	 * @param strList
	 * @param maxLen
	 * @return
	 */
	public static char[][] convertStrListToCharArr (List<String> strList, int maxLen) {
		if (strList == null || strList.size() == 0) {
			throw new IllegalArgumentException("Invalid list is input.");
		}
		
		if (maxLen == 0) {
			throw new IllegalArgumentException("Invalid max length value is input.");
		}
		
		char[][] char2dArr = new char[strList.size()][maxLen];
		
		int i = 0;
		for (String str: strList) {
			for (int j=0; j<str.length(); j++) {
				char2dArr[i][j] = str.charAt(j);
			}
			i++;
		}
		
		return char2dArr; 
	}
	
	/**
	 * This method is used to print out the found result in the queue. 
	 * @param queue
	 * @param targetType
	 */
	public static void printResults (Queue<Result> queue, String targetType) {
		if (queue == null || targetType == null || targetType.length() == 0) {
			throw new IllegalArgumentException("Invalid value(s) input");
		}
		
		// Print out the results
		System.out.println("Found " + targetType + " Images: " + queue.size());
		while (!queue.isEmpty()) {
			System.out.println("===========================");
			Result res = queue.poll();
			System.out.println("Target Type: " + res.getTargetType());
			System.out.println("Coordinates: (" + res.getCoordinate().getX() + ", " + res.getCoordinate().getY() +")");
			System.out.println("Match Rate: " + (int)res.getMatchRate() +"%");
		}
		System.out.println("===========================");
	}
}
