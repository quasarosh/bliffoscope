package bliffoscope.utils;

import java.util.Queue;

import bliffoscope.pojos.Coordinate;
import bliffoscope.pojos.Image;
import bliffoscope.pojos.Result;

/**
 * Utility file containing Image related utility methods. 
 * @author Rosh Lee
 *
 */
public class ImageUtils {
	/**
	 * This method is used to create a new Image object which contains only partial information of a big image 
	 * by using big image's coordinate and size information. 
	 * @param image
	 * @param coordinate
	 * @param width
	 * @param height
	 * @return
	 */
	public static Image getImageByCoordinateAndSize (Image image, Coordinate coordinate, int width, int height) {
		if (image == null || coordinate == null || width == 0 || height == 0) {
			throw new IllegalArgumentException("Invalid value(s) input");
		}
		
		char[][] pixels = new char[height][width];
		int x = coordinate.getX();
		int y = coordinate.getY();
		
		for (int i=0; i<height && x<image.getHeight(); i++,x++) {
			y = coordinate.getY();
			for (int j=0; j<width && y<image.getWidth(); j++,y++) {
				pixels[i][j] = image.getPixels()[x][y];
			}
		}
		
		return new Image(width, height, pixels);
	}
	
	/**
	 * This method is used to draw (or print out) a found images in the queue. 
	 * @param resQueue
	 * @param test
	 * @param target
	 */
	public static void drawFoundImages (Queue<Result> resQueue, Image test, Image target) {
		if (resQueue == null || test == null || target == null) {
			throw new IllegalArgumentException("Invalid value(s) input");
		}
		
		int i=1;
		while (!resQueue.isEmpty()) {
			Result res = resQueue.poll();
			System.out.println("== Image #" + i++ +" ==");
			System.out.println("Match Rate: " + (int)res.getMatchRate() + "%");
			System.out.println("x: " + res.getCoordinate().getX() + ", y: " + res.getCoordinate().getY());
			
			Coordinate coordinate = new Coordinate(res.getCoordinate().getX(), res.getCoordinate().getY());
			
			Image resImage = ImageUtils.getImageByCoordinateAndSize(test, coordinate, target.getWidth(), target.getHeight());
			resImage.drawImage();
		}
	}
}
