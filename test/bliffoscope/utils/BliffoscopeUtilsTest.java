package bliffoscope.utils;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertArrayEquals;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

/**
 * Unit tests for BliffscopeUtils.java file. 
 * @author Rosh Lee
 *
 */
public class BliffoscopeUtilsTest {

	@Before
	public void setUp() throws Exception {

	}

	/**
	 * Positive test for extractTotalOnsPerLine method.
	 */
	@Test
	public void testExtractTotalOnsPerLine() {
		String str = "string+with+plus";
		int expect = 2;
		int result = BliffoscopeUtils.extractTotalOnsPerLine(str);
		assertEquals(expect, result);
		
	}

	/**
	 * Negative tests for extractTotalOnsPerLine method.
	 * Exception handling
	 */
	@Test(expected=IllegalArgumentException.class)
	public void testExtractTotalOnsPerLineIllegalArgumentException() {
		// Testing with null value
		String str = null;
		BliffoscopeUtils.extractTotalOnsPerLine(str);
		
		// Testing with a string having 0 length
		str = "";
		BliffoscopeUtils.extractTotalOnsPerLine(str);
	}
	
	/**
	 * Positive test for convertStrListToCharArr method.
	 */
	@Test
	public void testConvertStrListToCharArr() {
		String[] strs = {"Unit", "Test"};
		
		char[][] charArrs = new char[2][4];
		charArrs[0] = strs[0].toCharArray();
		charArrs[1] = strs[1].toCharArray();
		
		List<String> list = new ArrayList<String>();
		list.add(strs[0]);
		list.add(strs[1]);
		char[][] resArr = BliffoscopeUtils.convertStrListToCharArr(list, 4);
		assertArrayEquals(charArrs, resArr);
		
	}
	
	/**
	 * Negative tests for convertStrListToCharArr method.
	 */
	@Test(expected=IllegalArgumentException.class)
	public void testConvertStrListToCharArrIllegalArgumentException() {
		// list value is null
		List<String> list = null;
		BliffoscopeUtils.convertStrListToCharArr(list, 4);
		
		// list has no element
		list = new ArrayList<String>();
		BliffoscopeUtils.convertStrListToCharArr(list, 4);
		
		// maxLen value is 0
		list.add("Testing...");
		BliffoscopeUtils.convertStrListToCharArr(list, 0);
	}
	
}
